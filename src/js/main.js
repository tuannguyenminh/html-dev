var handmarkObj = (function() {
	
	var filterBtn  = $('.filter-btn'),
		filterMenu = $('div.filter--menu'),
		modCollapseHeader = $('.module-collapse .module-header')


		;


	function init() {
		
		homeBannerSlider();
		blogBannerSlider();
		//bannerParallax();

		filterBtn.click(function(e){
			e.preventDefault();
			filterBtnHandle();
		});

		moduleCollapse();
		menuCollapse();
		cateSlider();


		prodThumbClick();
		

		loginModalform();
		

	}


	//preLoader
	function filterBtnHandle(){


		if( filterBtn.hasClass('active') && filterMenu.hasClass('active')){
			filterBtn.removeClass('active');
			filterMenu.removeClass('active');
		}else{
			filterBtn.addClass('active');
			filterMenu.addClass('active');
		}

	}

	function homeBannerSlider(){
		if($('body').hasClass('home')){

			$('.banner-slider').slick({
				dots: true
			});

			// $('.about-slick-wrap').slick({
			// 	dots: false,
  	// 			infinite: true,
  	// 			arrows: false
			// });

			// $('.banner-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// 	$('.about-slick-wrap').slick('slickNext'); 
			// });
		}
	}


	function blogBannerSlider(){
		if($('body').hasClass('blog')){
			$('.blog-slider-wrap').slick({
				autoplay: true,
  				autoplaySpeed: 2000
			});

		}
	}
	

	function cateSlider(){
		if($('body').hasClass('cate')){
			$('.cate-slider').slick({
				autoplay: true,
  				autoplaySpeed: 2000,
  				slidesToShow: 4,
  				slidesToScroll: 1,

  				  responsive: [
					


				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow:4,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    }
				    // You can unslick at a given breakpoint now by adding:
				    // settings: "unslick"
				    // instead of a settings object
				  ]


			});

		}
	}



	function bannerParallax(){
		 	
		 	if($(".parallax").length < 1) return;

		 	var parallax = document.querySelectorAll(".parallax"),
		  	speed = 0.5;

			  window.onscroll = function(){
			    [].slice.call(parallax).forEach(function(el,i){

			      var windowYOffset = window.pageYOffset,
			          elBackgrounPos = "50% " + (windowYOffset * speed) + "px";

			      el.style.backgroundPosition = elBackgrounPos;

			    });
			  };

	}


	function moduleCollapse(){

			
		$('.module-collapse').each(function(){

			var self = $(this);
			var modHeader= self.find('.module-header');
			var modContent = self.find('.module-content');

			modHeader.on('click', function(e){
				modContent.slideToggle();
			});

		});

	}


	function menuCollapse(){
		$('.menu-collapse').each(function(){
			
			var item = $(this).find('> li.has-child > a');

			// hide all links except for the first
			$('ul.sub:not(:first)').hide();
			$('ul.sub:first').parent().addClass('active');



			item.on('click', function(e){
				e.preventDefault();
			

				
			
				if( $(this).parent().hasClass('active')){

					$('.menu-collapse').find('li.has-child').removeClass('active');
					$('ul.sub').slideUp('slow');


				}else{

					$('.menu-collapse').find('li.has-child').removeClass('active');
					$('ul.sub').slideUp('slow');

					$(this).next().slideDown();
					$(this).parent().addClass('active');
				}



				return false;

			});





		});
		
	}

	function prodThumbClick(){

		$('.product-img-thumb > li > a').each(function(){

			var self = $(this);
			self.on('click', function(e){
				e.preventDefault();
				var imgSrc = self.data('large');

				//$('.product-img-large').find('img').attr('src', imgSrc);
				$('.product-img-large').find('img').fadeOut(300, function(){

				      $(this).attr('src',imgSrc ).bind('onreadystatechange load', function(){
				         if (this.complete) $(this).fadeIn(500);
				      });
				   });

			});

		});

	}



	function loginModalform(){

		var btnReg = $(".btn-reg");



		btnReg.on('click', function(e){
			e.preventDefault();

			$('.login-form').hide("slow");
			$('.register-form').show("slow");

		});

		$('.login-toggle').on('click', function(e){
			e.preventDefault();

			$('.login-form').show("slow");
			$('.register-form').hide("slow");

		});


		$('.btn-cancel').on('click', function(e){
			e.preventDefault();
			$('#loginModal').modal('hide');
		});

	}





	return { 
		init : init 
	};
	

})();

handmarkObj.init();
